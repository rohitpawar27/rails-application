# Use the official Ruby image as base
FROM ruby:2.7.4

# Install dependencies
RUN apt-get update -qq && apt-get install -y postgresql-client

# Set the application directory
WORKDIR /app

# Install gems
COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle config set --local without 'development test' && bundle install

# Copy the main application
COPY . ./

# Start the server
CMD ["rails", "server", "-b", "0.0.0.0"]

